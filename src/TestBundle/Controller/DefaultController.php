<?php

namespace TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller {

    public function indexAction() {
        $youtube = $this->youtubeAction();

        return $this->render( 'TestBundle:Default:index.html.twig', array( 'youtube' => $youtube ) );
    }

    public function youtubeAction($nr = 8, $ch_id = 'UC_AIMAVfCzHkEs0eIklHBCw') {
        $client = new \Google_Client();
        $client->setApplicationName( "Client_Youtube_Examples" );
        $apiKey = "AIzaSyAxCvglPzZVEyavoTzyzqsvmoSYy_Xuprc";
        $client->setDeveloperKey( $apiKey );
        $service        = new \Google_Service_YouTube( $client );
        $searchResponse = $service->search->listSearch( 'id,snippet', array(
            'channelId' => $ch_id,
            'maxResults' => $nr,
            'type'=> 'video'
        ));
        $videos         = [];
        foreach ( $searchResponse['items'] as $searchResult ) {
                    $vidId    = $searchResult['id']['videoId'];
                    $dur      = file_get_contents( "https://www.googleapis.com/youtube/v3/videos?part=contentDetails&id=$vidId&key=AIzaSyAxCvglPzZVEyavoTzyzqsvmoSYy_Xuprc" );
                    $duration = json_decode( $dur, true );
                    $vduration = $this->time($duration);
                    $videos[]  = array(
                        "title"       => $searchResult['snippet']['title'],
                        "video_id"    => $vidId,
                        "description" => $searchResult['snippet']['description'],
                        "duration"    => $vduration,
                    );
        }

        return array(
            'youtube_videos' => $videos
        );
    }

    public function time($duration) {
        preg_match_all("/PT(\d+H)?(\d+M)?(\d+S)?/", $duration['items'][0]['contentDetails']['duration'], $parts );
        $hours   = strlen($parts[1][0]) == 0 ? 0 : substr($parts[1][0], 0, strlen($parts[1][0]) - 1);
        $minutes = strlen($parts[2][0]) == 0 ? 0 : substr($parts[2][0], 0, strlen($parts[2][0]) - 1);
        $seconds = strlen($parts[3][0]) == 0 ? 0 : substr($parts[3][0], 0, strlen($parts[3][0]) - 1);
        $vduration = ( sprintf( "%02d", $hours ) . ":" . sprintf( "%02d", $minutes ) . ":" . sprintf( "%02d", $seconds ) );
        return $vduration;
    }

    /**
     * @Route("/ajax", name="ajax")
     *  * @Template()
     */
    public function ajaxAction(Request $request) {
        $nr = $request->request->get('nr');
        $ch_url = $request->request->get('ch_url');
        if (filter_var($ch_url, FILTER_VALIDATE_URL) === FALSE) {
            $videos = $this->youtubeAction($nr);
        }
        else {
            $ch_id = $this->get_youtube_channel_ID($ch_url);
            $videos = $this->youtubeAction($nr, $ch_id);
        };
        $begin = $request->request->get('begin');
        $end = $request->request->get('end');

        return $this->render( 'TestBundle:Default:youtube.html.twig',
            array ( 'youtube' => $videos , 'begin' => $begin , 'end' => $end));
    }

    public function get_youtube_channel_ID($ch_url){
        $html = file_get_contents($ch_url, null, null);
        preg_match("'<meta itemprop=\"channelId\" content=\"(.*?)\"'si", $html, $ch_id);
        if($ch_id && $ch_id[1]);
        return $ch_id[1];
    }

    /**
     * @Route("/ajax_ch", name="ajax_ch")
     *  * @Template()
     */
    public function ajax_chAction(Request $request) {
        $ch_url = $request->request->get('ch_url');
        if (filter_var($ch_url, FILTER_VALIDATE_URL) === FALSE) {
            $videos = $this->youtubeAction(8);
        }
        else {
            $ch_id = $this->get_youtube_channel_ID($ch_url);
            $videos = $this->youtubeAction(8, $ch_id);
        };
        return $this->render( 'TestBundle:Default:search.html.twig',
            array ( 'youtube' => $videos ) );
    }
}
